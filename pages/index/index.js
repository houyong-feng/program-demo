//index.js
//获取应用实例
const app = getApp();
import { request } from "../../request/index";
//Page Object
Page({
  data: {
    swiperList: [],
    categoryList: [],
    floorList: [],
  },
  //options(Object)
  // 页面加载的时候执行
  onLoad: function (options) {
    // request
    // 获取轮播图
    this.getSwiperList();
    // 分类导航
    this.getCategoryList();
    // 楼层
    this.getfloorList();
  },

  // 获取楼层
  getfloorList() {
    request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/home/floordata",
    }).then((result) => {
      this.setData({
        floorList: result.data.message,
      });
    });
  },

  // 获取轮播图
  getSwiperList() {
    request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata",
    }).then((result) => {
      this.setData({
        swiperList: result.data.message,
      });
    });
  },

  // 获取导航分类
  getCategoryList() {
    request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/home/catitems",
    }).then((result) => {
      this.setData({
        categoryList: result.data.message,
      });
    });
  },
  //item(index,pagePath,text)
  onTabItemTap: function (item) {},
});
