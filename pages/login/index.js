// pages/login/index.js

import { request } from "../../request/index";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    code:''
  },
  // 登陆
  login(e) {
    // 
    request({
      method: 'POST',
      url: "/user/login/wechat",
      data: {
        ...e.detail,
        code: this.data.code
      }
    }).then((result) => {
      console.log(result);
      if (result.code === "200") {
        wx.setStorageSync("userInfo", { time: Date.now(), data: result.data });
        this.setData({
          userInfo: result.data
        })
      } else {
        // TODO 错误提示
      }
    });

  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //  页面加载的时候获取新的code
    wx.login({
      timeout: 10000,
      success: (result) => {
        this.setData({
          code: result.code
        })
      },
      fail: (e) => { reject(e) },
      complete: () => { }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})